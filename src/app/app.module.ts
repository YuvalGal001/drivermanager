import { BrowserModule } from '@angular/platform-browser';
import { NgModule  } from '@angular/core';
import {MetrialModule} from "./shared/metrial.module";
import { AppComponent } from './app.component';
import { NavbarComponent } from './navbar/navbar.component';
import {RouterModule} from '@angular/router';
import { HomeComponent } from './home/home.component';
import { HttpClientModule } from '@angular/common/http';
import {StudentsModule} from './students/students.module';
@NgModule({
  declarations: [
    HomeComponent,
    AppComponent,
    NavbarComponent,

    
   
  ],
  imports: [
    BrowserModule,
    MetrialModule,
    RouterModule.forRoot([
      {path:"", component:HomeComponent},
    ]),
    StudentsModule,
    HttpClientModule


  ],
  providers: [],
  bootstrap: [AppComponent],

})
export class AppModule { }
