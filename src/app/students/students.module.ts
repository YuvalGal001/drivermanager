import { BrowserModule } from '@angular/platform-browser';
import { NgModule  } from '@angular/core';
import {RouterModule} from '@angular/router';
import { StudentListComponent } from './student-list/student-list.component';
import { StudentComponent } from './student-list/student/student.component';
import { HttpClientModule } from '@angular/common/http';
import { MetrialModule } from '../shared/metrial.module';
@NgModule({
  declarations: [
    StudentListComponent,
    StudentComponent
  ],
  imports: [
    BrowserModule,
    MetrialModule,
    RouterModule.forChild([
      {path:"studentList",component:StudentListComponent}
    ]),
    HttpClientModule


  ],
  providers: [],
  bootstrap: [],
})
export class StudentsModule { }
