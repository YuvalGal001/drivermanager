import { student } from './student/student.model';
import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import { of } from 'rxjs';
import { observable } from 'rxjs';
@Injectable({
  providedIn: 'root'
})
export class StudentsService {
  apiUrl = "./assets/studentFakeData.json";
  constructor(private http:HttpClient) { }

  fetchStudents(){
    return this.http.get<student[]>(this.apiUrl);
  }
}
