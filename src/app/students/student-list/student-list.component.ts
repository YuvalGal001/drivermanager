import { student } from './student/student.model';
import { StudentsService } from './students.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-student-list',
  templateUrl: './student-list.component.html',
  styleUrls: ['./student-list.component.css']
})
export class StudentListComponent implements OnInit {

  constructor(private studentsService:StudentsService) { }
  stduents:student[] = [];
  ngOnInit() {
  this.studentsService.fetchStudents().subscribe((response) => {
     this.stduents = response;
    })
  }

}
